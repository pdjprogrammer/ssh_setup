#!/bin/bash

# Check if running as root or using sudo, if not exit script
if [ "$EUID" -ne 0 ]; then
    echo "This script must be run with sudo."
    exit 1
fi

# The username for which to configure passwordless sudo
cur_user="username"

# Create a new file in /etc/sudoers.d with the appropriate configuration
echo "$cur_user ALL=(ALL) NOPASSWD:ALL" | tee /etc/sudoers.d/$cur_user > /dev/null

# Set the correct permissions for the sudoers.d file
chmod 0440 /etc/sudoers.d/$cur_user

echo "Passwordless sudo configured for user $cur_user."
