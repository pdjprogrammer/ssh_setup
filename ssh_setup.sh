#!/bin/bash

# Check if running as root or using sudo, if not exit script
if [ "$EUID" -ne 0 ]; then
    echo "This script must be run with sudo."
    exit 1
fi

# Make sure proper packages are installed
echo 'Installing policycoreutils-python-utils...'
dnf install -yq policycoreutils-python-utils >/dev/null 2>&1

# Set some variables: current user, random port, sshd_config file
cur_user="username"
new_ssh_port="$((RANDOM % 10001 + 30000))"
sshd_config="/etc/ssh/sshd_config.d/00-$cur_user.conf"

echo 'Creating custom user config for sshd...'
touch $sshd_config

# Make sure new config file was created, if not exit script
if [ ! -e $sshd_config ]; then
    echo "Failed to create new config file"
    exit 1
fi

# Force sshd to use only ed25519 host key
sh -c "echo 'HostKey /etc/ssh/ssh_host_ed25519_key' >> $sshd_config"

# Do not allow root to login via ssh
sh -c "echo 'PermitRootLogin no' >> $sshd_config"

# Allow publickey authentication
sh -c "echo 'PubkeyAuthentication yes' >> $sshd_config"

# Do not allow password authentication or empty passwords
sh -c "echo 'PasswordAuthentication no' >> $sshd_config"
sh -c "echo 'PermitEmptyPasswords no' >> $sshd_config"

# Only allow specific users to access ssh
sh -c "echo 'AllowUsers $cur_user' >> $sshd_config"

# Only allow publickey authentication and no other authentication
sh -c "echo 'AuthenticationMethods publickey' >> $sshd_config"

# Log authentication fails after 3 tries
sh -c "echo 'MaxAuthTries 3' >> $sshd_config"

# Change port sshd listens on
sh -c "echo 'Port $new_ssh_port' >> $sshd_config"

# Inform SELinux of port change
echo 'Changing SELinux port for ssh_port_t...'
semanage port -a -t ssh_port_t -p tcp $new_ssh_port
semanage port -l | grep ssh

# Change firewall settings
echo 'Changing firewall settings for ssh new port...'
firewall-cmd --permanent --zone=public --add-port=$new_ssh_port/tcp >/dev/null 2>&1
firewall-cmd --permanent --remove-service=ssh >/dev/null 2>&1
firewall-cmd --reload >/dev/null 2>&1

# Restart sshd
echo 'Restarting sshd with new configuration...'
echo "Your new port to connect via ssh is $new_ssh_port"
systemctl restart sshd
