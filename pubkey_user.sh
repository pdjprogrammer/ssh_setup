#!/bin/bash

# Check if running as root or using sudo
if [ "$EUID" -eq 0 ]; then
    echo "This script must be ran as a normal user."
    exit 1
fi

mkdir -pv ~/.ssh
chmod -v 0700 ~/.ssh
touch ~/.ssh/authorized_keys
chmod -v 0600 ~/.ssh/authorized_keys
echo "Please enter your id_ed25519.pub key: "
read user_input

# Store the input in a variable
pubkey="$user_input"

# Now you can use the input_variable as needed
sh -c "echo '$pubkey' >> ~/.ssh/authorized_keys"

# Change username in other script file
sed -i "s/\"username\"/\"$USER\"/" ssh_setup.sh
sed -i "s/\"username\"/\"$USER\"/" sudo_setup.sh
